"""unit test for uncertainties."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest


def test_uncertainty_vectors_repeat(comp_contribution, reaction_dict):
    """Test the different formats of the uncertainty matrix."""
    reactions = [reaction_dict["atpase"], reaction_dict["atpase"]]

    (
        mu,
        sigma_fin,
        sigma_inf,
        sigma_res,
    ) = comp_contribution.preprocess.get_reaction_prediction_multi(reactions)

    assert mu.shape == (2,)
    assert sigma_fin.shape == (2, 590)
    assert sigma_inf.shape == (2, 79)
    assert sigma_res.shape == (2, 0)

    cov_fin = sigma_fin @ sigma_fin.T
    cov_inf = sigma_inf @ sigma_inf.T
    cov_res = sigma_res @ sigma_res.T

    assert cov_fin == pytest.approx(0.0926, rel=1e-2)
    assert cov_inf == pytest.approx(0)
    assert cov_res == pytest.approx(0)


def test_uncertainty_vectors(comp_contribution, reaction_dict):
    """Test the different formats of the uncertainty matrix."""
    reactions = [
        reaction_dict["unresolved_0"],
        reaction_dict["unresolved_1"],
        reaction_dict["unresolved_2"],
    ]

    (
        mu,
        sigma_fin,
        sigma_inf,
        sigma_res,
    ) = comp_contribution.preprocess.get_reaction_prediction_multi(reactions)

    assert mu.shape == (3,)
    assert sigma_fin.shape == (3, 590)
    assert sigma_inf.shape == (3, 79)
    assert sigma_res.shape == (3, 2)

    cov_fin = sigma_fin @ sigma_fin.T
    cov_inf = sigma_inf @ sigma_inf.T
    cov_res = sigma_res @ sigma_res.T

    assert cov_fin[0, :] == pytest.approx([70.1, 0.773, -0.228], rel=1e-2)
    assert cov_fin[1, :] == pytest.approx([0.773, 71.1, 0.758], rel=1e-2)
    assert cov_fin[2, :] == pytest.approx([-0.228, 0.758, 1.21], rel=1e-2)

    assert cov_inf[0, :] == pytest.approx([5, 0, 0])
    assert cov_inf[1, :] == pytest.approx([0, 0, 0])
    assert cov_inf[2, :] == pytest.approx([0, 0, 0])

    assert cov_res[0, :] == pytest.approx([0, 0, 0])
    assert cov_res[1, :] == pytest.approx([0, 2, 2])
    assert cov_res[2, :] == pytest.approx([0, 2, 2])
